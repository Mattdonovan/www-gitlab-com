---
layout: markdown_page
title: "Category Direction - Release Governance"
---

- TOC
{:toc}

## Release Governance

Release Governance is about addressing the demand of the business to understand what is changing in your software. Our focus is on supporting the variety of controls and automation (security, compliance, or otherwise) to ensure your releases are managed in an auditable and trackable way. 

The backbone of this category is creating a single artifact for our users to furnish during an audit or compliance process. The strong integration across GitLab enables the creation of an auditable chain of custody for assets, commits, issues, including satisfactorily meeting quality and security gates. Table stakes for enterprise-grade governance includes traceability of automated actions alongside the gathering of appropriate approvals throughout the release process. Our intention is to streamline the experience of preparing for an audit or compliance review as an organic byproduct of using GitLab.

Release Governance is complemented by the tangential category within Release of [Secrets Management](/direction/release/secrets_management). Also related is [Requirements Management](/direction/plan/requirements_management) from the [Plan](/direction/plan/) stage.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARelease%20Governance)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)

## What's Next & Why

Now that we have delivered the first snapshot of Release Evidence ([gitlab#26019](https://gitlab.com/gitlab-org/gitlab/issues/26019)), we are expanding the snapshot to be at time of release end date in [gitlab#38103](https://gitlab.com/gitlab-org/gitlab/issues/38103). By making both of these snapshots available in the release itself, we help tell the story of  what, where, when, and by whom any change in the software happened, without any additional effort. Up next, we are expanding our compliance requirements to include test results within release evidence via [gitlab#2379](https://gitlab.com/groups/gitlab-org/-/epics/2379). 

A consistent challenge across GitLab users is being able to track deployments to GitLab.com, which is a  focus for [gitlab#1936](https://gitlab.com/groups/gitlab-org/-/epics/1936). 
 
We have learned there is demand to support the logging auto-stop actions ([gitlab#36407](https://gitlab.com/gitlab-org/gitlab/issues/36047)). We are continuing to investigate the needs around access control by users and roles for protected enviornments in [gitlab#36665](https://gitlab.com/gitlab-org/gitlab/issues/36665).


## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is Viable (see our [definitions of maturity levels](/direction/maturity/)).

Key deliverables toward the next maturity target of Viable:

- [Collect release evidence at moment of release end date](https://gitlab.com/gitlab-org/gitlab/issues/38103) (12.8)
- [Deploy Freezes for environments MVC](https://gitlab.com/gitlab-org/gitlab/issues/24295) (12.10)
- [Binary Authorization MVC](https://gitlab.com/gitlab-org/gitlab/issues/7268)

## Competitive Landscape

A key capability of products which securely manage releases is to collect
evidence associated with releases in a secure way. In [gitlab#26019](https://gitlab.com/gitlab-org/gitlab/issues/26019)
we introduced a new kind of entity that is part of a release, which contains
various kinds of evidence. As we expand the evidence to include test results via [gitlab#32773](https://gitlab.com/gitlab-org/gitlab/issues/32773), security scans, and other artifacts in [gitlab#2207](https://gitlab.com/groups/gitlab-org/-/epics/2207) that
can be collected as part of a release generation, is a great differentiator for GitLab. This will uniquely enable us to be a single application for the DevOps lifecycle.

## Analyst Landscape

The analysts in this space tend to focus a lot right now on existing, more
legacy style deployment workflows so changes like [gitlab#15819](https://gitlab.com/gitlab-org/gitlab/issues/15819) 
(better support for validation of approvals in the pipeline) will help
us perform better here, as well as for the kinds of customers who are
really need a bit more control over their delivery process.

Similarly, integrations with technologies like ServiceNow ([gitlab#8373](https://gitlab.com/gitlab-org/gitlab/issues/8373))
will help GitLab fit in better with larger, pre-existing enterprise governance workflows.

## Top Customer Success/Sales Issue(s)

Sales and prospects have communicated an interest in expanding more granular permissions within protected environments as solved by [gitlab#15819](https://gitlab.com/gitlab-org/gitlab/issues/15819). 

The CS team frequently sees requests for integration with ServiceNow to support change
management in CD pipelines, as per [gitlab#8373](https://gitlab.com/gitlab-org/gitlab/issues/8373).

## Top Customer Issue(s)

Enabling the upload of assets to releases [gitlab#17838](https://gitlab.com/gitlab-org/gitlab/issues/17838), within release evidence satifies many customers' needs to show the entirety of release and further strengthens the Release page as a single source of truth.  

Another issue referenced by a handful of customers interested in enterprise-grade governance, includes enforcing the signtaure validation of containers during deployment as captured in our [Binary Authorization MVC](https://gitlab.com/gitlab-org/gitlab/issues/7268). 

## Top Internal Customer Issue(s)

Implementing better practices and expanding the use case documentation of [gitlab#15819](https://gitlab.com/gitlab-org/gitlab/issues/15819) will help address many of the internal requests for gating deployments. 

### Features of Interest

 - [Releases Page](https://gitlab.com/gitlab-org/gitlab-ce/releases)
 - [Approval Jobs](https://gitlab.com/gitlab-org/gitlab/issues/15819) 
 - [Release Evidence](https://gitlab.com/gitlab-org/gitlab/issues/26019) 

### Ongoing efforts to support

 - [Internal change management process](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/231)
 - [Service lifecycle workflow](/handbook/engineering/security/guidance/SLC.1.01_service_lifecycle_workflow.html)
 - [Controls by family](/handbook/engineering/security/sec-controls.html#list-of-controls-by-family)

## Top Vision Item(s)

Important for this category (though also expansive and includes a few others) is
our epic for [locking down the path to production](https://gitlab.com/groups/gitlab-org/-/epics/762),
which will help us successfully deliver compliance controls within the software delivery pipeline.

Also related to locking down the path to production is binary
authorization ([gitlab#7268](https://gitlab.com/gitlab-org/gitlab/issues/7268))
which provides a secure means to validate deployable containers. At the
moment however this only works with GKE so ultimate user adoption is limited. As such we're
keeping an eye on adoption, but have not yet implemented an MVC.

Deploy freezes ([gitlab#24295](https://gitlab.com/gitlab-org/gitlab/issues/24295))
will help compliance teams enforce periods where production needs to remain stable/not change. We have conducted our research of deploy freezes in [gitlab#39108](https://gitlab.com/gitlab-org/gitlab/issues/39108) and found many users have windows of time they manually enforce. Being able to support these workflows in GitLab will reduce risk and save time for these users. 
