---
layout: markdown_page
title: "Database Sharding Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | February 11, 2020 |
| Target End Date | August 11, 2020|
| Slack           | [#wg_database-sharding](https://gitlab.slack.com/archives/CTNSZFHEZ) (only accessible from within the company) |
| Google Doc      | [Database Sharding Working Group Agenda](https://docs.google.com/document/d/1_sI-P2cLYPHlzDiJezI0YZHjWAC4BSKJ8aL0cNduDlo/edit#) (only accessible from within the company) |
| Issue Board     | TBD             |

## Business Goal

The initial phase of the working group will be to level set on the goals of the working group, communicate what is expected from each stakeholder, formulate a timeline and detail success criteria.  The ultimate goal is to improve our scalability and performance by implementing partitioning and sharding solutions for our SAAS and self-managed customers.  

## Exit Criteria

## Specific Lines of Enquiry

* [Upgrade to PostgreSQL 11 timeline](https://gitlab.com/groups/gitlab-org/-/epics/2184)
* [Infrastructure - Upgrade to PostgreSQL 11](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/6505)
* Testing PostgreSQL 11 upgrade
* [Database Paritioning](https://gitlab.com/groups/gitlab-org/-/epics/2023)
* [Database Sharding](https://gitlab.com/groups/gitlab-org/-/epics/1854)

## Roles and Responsibilities

| Working Group Role                       | Person                          | Title                                    |
|------------------------------------------|---------------------------------|------------------------------------------|
| Executive Stakeholder | Christopher Lefelhocz           | Senior Director of Development           |
| Facilitator | Craig Gomes | Engineering Manager, Database            |
| DRI for Database Sharding | Craig Gomes | Engineering Manager, Database            |
| Functional Lead | TBD | Quality Engineering Manager, Dev         |
| Functional Lead | Josh Lambert | Senior Product Manager, Geo              |
| Functional Lead | Gerardo "Gerir" Lopez-Fernandez | Engineering Fellow, Infrastructure       |
| Functional Lead | Stan Hu                         | Engineering Fellow, Development          |
| Functional Lead | Andreas Brandl                  | Staff Backend Engineer, Database            |
| Member  | Chun Du | Director of Engineering, Enablement      |
| Member  | Pat Bair | Senior Backend Engineer, Database          |
