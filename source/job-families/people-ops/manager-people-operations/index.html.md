---
layout: job_family_page
title: Senior Manager, People Operations
---

Manages the People Experience team, People Operations Specialist team and People Operations Full Stack Engineer team

## Responsibilities
* Onboard, mentor, and grow the careers of all team members
* Provide coaching to improve performance of team members and drive accountability
* Work with Director, People Operations to shape a strategy that aligns and moves GitLab towards continued growth, innovation and improvement
* Provide training and support to the Specialist group to address team member and leadership queries effectively and timely
* Oversee the co-employer relationships and act as the main point of contact for the People Group
* Partner with Finance and Legal to establish entities or co-employers in new countries 
* Seek and review potential blockers of various People Operations processes and ensuring improvement on daily tasks and suggest automation where needed
* Improve Slack answers and continue to train and mentor the team to update information in the handbook, for easy access to all
* Implement an SLA to track response times for email queries across timezones
* Continue to drive automation for easy access of employment confirmation letters, automatic invitations to Contribute, confirmation of business insurance, etc
* Implementation of data retention strategies and partnering with People Leads to ensure this is implemented across all team member data consistently
* Continuous Improvement of both administration and people experience during onboarding.
* Partnering closely with Managers at GitLab to gain feedback from various teams about onboarding, offboarding and transition issues
* Review and report on People Operations and Onboarding metrics, including [OSAT](/handbook/people-group/people-group-metrics/#onboarding-satisfaction-osat) and [Onboarding Task Completion](/handbook/people-group/people-group-metrics/#onboarding-task-completion--x-tbd)
* Continuous improvement of offboarding for both voluntary and involuntary terms
* Along with the People Business Partners, create a Terminations playbook to ensure the team can remain async and scale
* Work closely with payroll, SecOps, and ITOps to continue to improve the administration of offboarding the the time it takes to complete


## Requirements
* Ability to use GitLab
* The ability to work autonomously and to drive your own performance & development would be important in this role
* Prior extensive experience in a People Operations role
* Willingness to work odd hours when needed (for example, to call an embassy in a different continent)
* Excellent written and verbal communication skills
* Exceptional customer service skills
* Strong team player who can jump in and support the team on a variety of topics and tasks
* Enthusiasm for and broad experience with software tools
* Proven experience quickly learning new software tools
* Willing to work with git and GitLab whenever possible
* Willing to make People Operations as open and transparent as possible
* Proven organizational skills with high attention to detail and the ability to prioritize
* You share our values, and work in accordance with those values
* The ability to work in a fast paced environment with strong attention to detail is essential
* High sense of urgency and accuracy
* Experience at a growth-stage tech company

## Performance Indicators
* 12 month team member retention
* 12 month voluntary team member turnover
* Onboarding TSAT > 4
* Onboarding task completion < X (TBD)

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to schedule a 45 minute interview with our People Operations Manager
* After that, candidates will be invited to schedule a 30 minute interview with members of the People Operations and People Partner teams
* After that, candidates will be invited to interview with the Director of People Operations
* Finally, our CPO may choose to conduct a final interview

Additional details about our process can be found on our [hiring page](/handbook/hiring).