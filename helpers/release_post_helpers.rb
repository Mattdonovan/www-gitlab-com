require 'hashie'

module ReleasePostHelpers
  def assemble_release_post(previous_milestone, next_milestone)
    current_article = OpenStruct.new
    current_article.data = OpenStruct.new
    current_article.data.title = "GitLab.com - What's New"
    current_article.data.description = "The latest features available on GitLab.com"
    current_article.data.previous_release_number = previous_milestone

    current_article.content = Hashie::Mash.new
    current_article.content.deep_merge(release_post_items('unreleased'))

    if !current_article.content.empty?
      current_article.data.release_number = next_milestone + " Preview"
    else
      current_article.data.release_number = previous_milestone
      _, current_article.content = data.release_posts.except("unreleased").max
    end
    current_article
  end

  def release_post_items(milestone)
    items = Hashie::Mash.new

    data.release_posts[milestone.tr(".", "_")].each do |file_name, content|
      next if file_name == "samples"

      items.deep_merge!(content) { |key, this_val, other_val| this_val + other_val }
    end

    items
  end
end
